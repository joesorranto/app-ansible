<?php

$api_key = '05e67cd3519ea1ab7a4ceaf8adfca9da';
$city = $_POST['city'];
$api_url = 
"http://api.openweathermap.org/data/2.5/weather?q={$city}&units=metric&appid={$api_key}&lang=ru";

$weather_data = json_decode(file_get_contents($api_url), true);

if ($weather_data['cod'] == 200) {
    $temperature = $weather_data['main']['temp'];
    $description = $weather_data['weather'][0]['description'];
} else {
    $error = 'Не удалось получить информацию о погоде. Проверьте введенные 
данные.';
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Погодные условия</title>
</head>
<body>
    <h1>Погода в городе <?php echo htmlspecialchars($city); ?></h1>
    <?php if (isset($error)): ?>
        <p><?php echo htmlspecialchars($error); ?></p>
    <?php else: ?>
        <p>Температура: <?php echo htmlspecialchars($temperature); ?> 
°C</p>
        <p>Описание: <?php echo htmlspecialchars($description); ?></p>
    <?php endif; ?>
    <a href="sites.php">Вернуться</a>
</body>
</html>
