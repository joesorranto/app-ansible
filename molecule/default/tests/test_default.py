import os
import pytest
import testinfra.utils.ansible_runner





def test_install_package(host):
    nginx = host.package('nginx')
    assert nginx.is_installed


def test_service_enabled(host):
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled


versions = ["7.2", "7.3", "7.4", "8.0"]
def get_service_name(version):
    return f"php{version}-fpm"


@pytest.mark.parametrize("version", versions)
def test_php_fpm_service_running_and_enabled(host, version):
    service_name = get_service_name(version)
    php_fpm_service = host.service(service_name)
    assert php_fpm_service.is_running
    assert php_fpm_service.is_enabled
